# Container Map

**NOTE:** Ports are _NOT_ exposed by default (with the exception of Traefik)

## Map

| Service Name                             | Container Name       | Host Port (if enabled)        | Container Port              | Accessible via Traefik |
| ---------------------------------------- | -------------------- | ----------------------------- | --------------------------- | ---------------------- |
| Portainer                                | `portainer`          | `9000`                        | `9443`                      | &#9745;                |
| Traefik                                  | `traefik`            | `90`,`9443`,`9090`            | `80`,`443`,`8080`           | &#9745;                |
| PiHole                                   | `pihole`             | `53`,`53/UDP`.`67/UDP`,`8000` | `53`,`53/UDP`.`67/UDP`,`80` | &#9744;                |
| Sabnzb                                   | `sabnzb`             | `8080`                        | `8080`                      | &#9745;                |
| Transmission-OpenVPN                     | `transmission`       | `8118`,`9091`                 | `8118`,`9091`               | &#9745;                |
| Tautulli                                 | `tautulli`           | `8182`                        | `8181`                      | &#9745;                |
| Mylar3                                   | `mylar`              | `8090`                        | `8090`                      | &#9745;                |
| Prowlarr                                 | `prowlarr`           | `9696`                        | `9696`                      | &#9745;                |
| Radarr                                   | `radarr`             | `7878`                        | `7878`                      | &#9745;                |
| Sonarr                                   | `sonarr`             | `8989`                        | `8989`                      | &#9745;                |
| Overseerr                                | `overseerr`          | `5055`                        | `5055`                      | &#9745;                |
| Tdarr                                    | `tdarr`              | `8265`,`8266`,`8267`          | `8265`,`8266`,`8267`        | &#9745;                |
| Organizr                                 | `organizr`           |                               |                             | &#9745;                |
| Lidarr                                   | `lidarr`             | `8686`                        | `8686`                      | &#9745;                |
| Readarr                                  | `readarr`            | `8787`                        | `8787`                      | &#9745;                |
| Flaresolverr                             | `flaresolverr`       | `8191`                        | `8191`                      | &#9745;                |
| Requestrr                                | `requestrr`          | `4545`                        | `4545`                      | &#9745;                |
| Calibre                                  | `calibre`            | `7080`,`7081`                 | `8080`,`8081`               | &#9745;                |
| Monitorr                                 | `monitorr`           | `9080`                        | `80`                        | &#9745;                |
| Homarr                                   | `homarr`             | `7575`                        | `7575`                      | &#9745;                |
| Watchtower                               | `watchtower`         |                               |                             |                        |